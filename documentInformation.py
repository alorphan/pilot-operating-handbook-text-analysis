class DocumentInformation:

    def __init__(self, document_name, authors, location):
        self.document_name = document_name
        self.authors = authors
        self.location = location
        #self.sentence_objects = []   # will use this once our program accepts multiple documents at a time
