import csv
import documentInformation

def to_csv(docInfo, totalTimeStr, costPerNounStr, total_nouns, unqNouns, sumNouns):
    # Open csv files to write to
    if docInfo.document_name != None:
        csv_name = docInfo.document_name + '_nouns.csv'
    else:
        csv_name = (docInfo.location.split('/'))[-1][:-4] + '_nouns.csv' # get name of file from file_path (removes ".pdf" too)

    with open(csv_name, 'w', newline='') as csvfile:
        nounwriter = csv.writer(csvfile)
        nounwriter.writerow([docInfo.document_name, docInfo.authors]) # can add more attributes too
        nounwriter.writerow(["Unique nouns: " + str(unqNouns), " Total nouns: " + str(sumNouns)])
        nounwriter.writerow([totalTimeStr, costPerNounStr])
        for noun in total_nouns:
            nounwriter.writerow([noun.text, noun.context_sentences, noun.num_occur])
        print('Data has been successfully saved to ' + csv_name)
    
    return csv_name